# Hyperbola Install Scripts

This is a small suite of scripts aimed at automating some menial
tasks when installing [Hyperbola GNU/Linux-libre](https://www.hyperbola.info).

## Requirements

* GNU coreutils (>= v8.15)
* util-linux (>= 2.23)
* POSIX awk
* bash (>= 4.1)

## License

See COPYING for details.
